# Cryptowall JS Decoer

This script simply takes a .js file from one of the zip files, decodes it and prints out the offending javascript so you can extract out the hosts that need to be blocked.

## Usage

Get the attachments from the email, save them, unzip the .js file and run

cw-js-decoder.py <jsfile>

You can give it multiple jsfiles as arguments and it'll decode each one.

