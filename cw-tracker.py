#!/usr/bin/env python
#
# To stop the warning about SSL you need to install
# libffi
# libffi-devel
# python-pyasn1
# python-ndg_httpsclient
# 

import sys
import re
import os
import requests
import logging
import base64

logging.getLogger(__name__).addHandler(logging.NullHandler())

BASEDIR = "/export/virus/pending"
chunk_size = 8196


class CryptoTracer:
    def __init__(self, basedir=None):
        if basedir:
            BASEDIR=basedir

    def url(self, hackedURL):
        """Follow the URL down the rabbit hole of crypto wall.

        Returns an array of URLs that were visited, their method, return status, a filepath to any files, and an Error field incase there was an error
        eg: [[ "http://xyx.com", "GET", "200", "/tmp/virus.zip", None ],]
        Errors can be:
        NETWORK_TIMEOUT - The network connection timed out
        REFRESHMATCH_FAILED - There was no meta refresh found on the first hop.
        """
        Results = []

        try:
            logging.info("Fetching '%s'" % hackedURL)
            response = requests.get(hackedURL, timeout=10.0)
            logging.info("Got response from site, searching for resfresh url")
            Results.append([response.url, response.status_code, response.request.method, None, None])
        except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout):
            logging.warning("Site timed out, might be down, or compromise detected.")
            Results.append([hackedURL, 'GET', None, None, "NETWORK_TIMEOUT"])
            return Results

        refreshUrl = re.search('<meta http-equiv=[\'"]refresh[\'"] content=[\'"]0; url=([^\'"]*)[\'"]', response.text)

        if not refreshUrl:
            # Set the last url to Error because we can't find the refresh we're expecting.
            logging.warning("Couldn't find a meta refresh in the response. aborting.")
            Results[-1][4] = "REFRESHMATCH_FAILED"
            return Results

        trojanURL = refreshUrl.groups()[0]

        if trojanURL.find("google.com") != -1:
            logging.warning("URL has detected us and is redirecting us to google....")
            Results[-1][4] = "SITE_HIDING_REDIRECTION_TO_GOOGLE"
            return Results

        try:
            # Send a post request trojan site with the captcha code (it's always the same number)
            logging.info("Found metarefresh URL of '%s' sending post data" % trojanURL)
            response = requests.post(trojanURL, data={"captcha_code": 26974}, timeout=10.0)
        except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout):
            logging.warning("Site timed out, might have been taken down..")
            Results.append([trojanURL, 'POST', None, None, "NETWORK_TIMEOUT"])
            return Results

        # Grab all the redirects we just passed through plus the current one.
        for res in list(response.history)+[response]:
            logging.info("Followed redirection to '%s'" % res.url)
            Results.append([res.url, res.status_code, "GET", None, None])

        # Check to see if we have been given an attachment if so, save it and add it to the last URL.
        if 'content-disposition' in response.headers and response.headers['content-disposition'].find("attachment") != -1:
            logging.info("Found binary file, downloading..")
            fname = response.headers['content-disposition'].split("=")[1]
            # Remove any UTF8 encoding markers
            fname = fname.replace("UTF-8''", "")
            fname = os.path.join(BASEDIR, fname)

            logging.info("Saving '%s'" % fname)
            with open(fname, 'wb') as fd:
                for chunk in response.iter_content(chunk_size):
                    fd.write(chunk)

            Results[-1][3] = fname
        else:
            Results[-1][4] = "NO_ATTACHMENT_FOUND"

        return Results


def main():
    logging.basicConfig(level=logging.INFO)

    logging.info("Starting the tracer...")
    trace = CryptoTracer()

    Results = []
    for url in sys.argv[1:]:
        # Store the results so we can print them all out at the end.
        Results.append(trace.url(url))
        
    for result in Results:
        print "Tracing %s" % result[0][0]
        print
        for url in result:
            if url[4]:
                print url[0], url[1], url[4]
            else:
                print url[0]
        if not url[4]:
            print "File saved as %s" % result[-1][3]
            print



if __name__ == '__main__':
    main()
