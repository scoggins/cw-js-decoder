#!/usr/bin/env python
# coding=utf-8
# Used to decode current cryptowall .js files
# -Simon Coggins <s.coggins@cqu.edu.au> 06/07/2015

import sys
import os
import re
import urllib

# See http://daringfireball.net/2010/07/improved_regex_for_matching_urls
urls = re.compile("""
(?xi)
([ '"])
(                       # Capture 1: entire matched URL
  (?:
    https?://               # http or https protocol
    |                       #   or
    www\d{0,3}[.]           # "www.", "www1.", "www2." … "www999."
    |                           #   or
    [a-z0-9.\-]+[.][a-z]{2,4}/  # looks like domain name followed by a slash
  )
  (?:                       # One or more:
    [^\s()<>\1'"]+                  # Run of non-space, non-()<>
    |                           #   or
    \(([^\s()<>]+|(\([^\s()<>]+\)))*\)  # balanced parens, up to 2 levels
  )+
  (?:                       # End with:
    \(([^\s()<>]+|(\([^\s()<>]+\)))*\)  # balanced parens, up to 2 levels
    |                               #   or
    [^\s`!()\[\]{};:'".,<>?«»“”‘’]        # not a space or one of these punct chars
  )
)
""", re.M)
formAction = re.compile("(?i)<form[^>]*action=['\"]?([^'\" ]+)['\"]?", re.M)
var = re.compile("var \w+\s*=\s*(?:'([^']+)'|\"([^\"]+)\")\s*;", re.M)


def skipCode(encoded):
    print "Checking for skipcode ... ",
    for x in range(2, 10):
        code = encoded[0::x]
        if code.find("function") != -1:
            print "%d char skip works" % x
            return code
    print "Not a skip code"
    return None


def xorCode(encoded):
    code = ""
    print "Checking for xor code ... ",
    for x in range(2, 10):
        for c in encoded:
            code += chr((ord(c) ^ x))

        if code.find("function") != -1:
            print "logical xor against %d" % x
            return code

    print "not xor code"
    return None


def decodeJavaScript(fname):
    jsCode = open(fname, "r").read()

    res = var.findall(jsCode)

    encoded = "".join(["".join(x) for x in res])


    for decoder in (skipCode, xorCode):
        code = decoder(encoded)
        if code:
            break
    if code:
        code = urllib.unquote(code)
        return code

    return None



def main():
    if len(sys.argv) == 1:
        print "Usage:\n%s <jsfile> [jsfile] ... " % sys.argv[0]
        sys.exit(0)
    for fname in sys.argv[1:]:
        if os.path.exists(fname):
            print "----------------- %s" % fname
            code = decodeJavaScript(fname)
            if code:
                code = code.replace("{", "{\n").replace("}", "}\n").replace(";", ";\n")
                print "---"
                print code
                URLs = urls.findall(code)
                forms = formAction.findall(code)
                if URLs:
                    print "----URLs---"
                    print "\n".join([i[1] for i in URLs])
                    if forms:
                        for form in forms:
                            print "Form Action: %s" % form


                print "------"

            else:
                print "Can't decode it"



if __name__ == '__main__':
    main()
